import React, { Component } from "react"
import { Text, KeyboardAvoidingView, View, StyleSheet, ImageBackground, ActivityIndicator } from "react-native"
import { Button } from "native-base"

import { withApollo } from "react-apollo"
import CreateUser from "./CreateUser"
import LoginUser from "./LoginUser"
import theme from "../../styles/theme"

class Login extends Component {
	state = {
		returningUser: true,
		isReady: false,
		loading: false
	}

	onSubmit = () => {
		this.setState({ loading: true })
	}

	render() {
		return (
			<View style={styles.container}>
				{this.state.loading && <ActivityIndicator size="large" color="#fff" style={styles.spinner} />}
				<ImageBackground resizeMode="cover" style={styles.image} source={require("../../assets/images/charlotte-skyline.jpg")} />
				<KeyboardAvoidingView behavior="position">
					{this.state.returningUser ? (
						<LoginUser {...this.props} onSubmit={this.onSubmit} />
					) : (
						<CreateUser {...this.props} onSubmit={this.onSubmit} />
					)}
					<Text style={styles.text}>{this.state.returningUser ? "Not a User?" : "Already a User?"}</Text>
					<Button
						block
						style={styles.button}
						onPress={() =>
							this.setState({
								returningUser: !this.state.returningUser
							})
						}
					>
						<Text style={styles.buttonText}>{this.state.returningUser ? "Register" : "Login"}</Text>
					</Button>
				</KeyboardAvoidingView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center"
	},
	button: {
		backgroundColor: theme.COLOR_ORANGE,
		margin: 20,
		marginTop: 10
	},
	buttonText: {
		color: "#fff",
		fontWeight: "bold"
	},
	text: {
		marginLeft: 20,
		color: "#fff"
	},
	image: {
		backgroundColor: theme.COLOR_GREEN,
		flex: 1,
		position: "absolute",
		width: "100%",
		height: "100%",
		alignItems: "center"
	},
	spinner: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		zIndex: 1000,
		backgroundColor: "rgba(0, 0, 0, 0.5)"
	}
})

export default withApollo(Login)
