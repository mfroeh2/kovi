import React, { Component } from "react"
import { StyleSheet, Text, View, Alert } from "react-native"
import { Form, Item, Input, Label, Button } from "native-base"
import theme from "../styles/theme"

export default class NoteForm extends Component {
	static defaultProps = {
		propertyNote: {}
	}

	state = {
		title: this.props.note ? this.props.note.title : "",
		body: this.props.note ? this.props.note.body : ""
	}

	submitForm = () => {
		this.props.onSubmit({
			title: this.state.title,
			body: this.state.body
		})
	}

	render() {
		const { toggleModal } = this.props
		return (
			<Form style={styles.form}>
				<Item floatingLabel style={styles.input}>
					<Label>Title</Label>

					<Input onChangeText={title => this.setState({ title })} value={this.state.title} />
				</Item>
				<Item floatingLabel style={styles.textareaContainer}>
					<Label>Note</Label>

					<Input numberOfLines={11} multiline onChangeText={body => this.setState({ body })} value={this.state.body} style={styles.textarea} />
				</Item>
				<View style={styles.buttonRow}>
					<Button block onPress={this.submitForm} style={[styles.button, styles.saveButton]}>
						<Text style={styles.buttonText}>Save</Text>
					</Button>

					<Button
						block
						onPress={() => {
							Alert.alert(
								"Are You Sure?",
								"All unsaved changes will be lost...",
								[{ text: "Close Anyway", onPress: toggleModal }, { text: "Stay in Note", onPress: () => "" }],
								{ cancelable: false }
							)
						}}
						style={styles.button}
					>
						<Text style={styles.buttonText}>Close</Text>
					</Button>
				</View>
			</Form>
		)
	}
}

const styles = StyleSheet.create({
	form: {
		paddingTop: 5,
		paddingBottom: 15,
		backgroundColor: "#fff",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 3
		},
		shadowOpacity: 0.27,
		shadowRadius: 4.65,

		elevation: 10
	},
	input: {
		marginRight: "5%"
	},
	textarea: {
		height: 230
	},
	textareaContainer: {
		paddingTop: 10,
		paddingBottom: 30,
		marginRight: "5%"
	},
	buttonRow: {
		flexDirection: "row",
		justifyContent: "space-around"
	},
	button: {
		marginTop: 20,
		width: "48%",
		backgroundColor: theme.COLOR_ORANGE
	},
	saveButton: {
		backgroundColor: theme.COLOR_GREEN
	},
	buttonText: {
		color: "#fff",
		fontWeight: "bold"
	}
})
