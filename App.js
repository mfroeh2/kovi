import React, { Component } from 'react';

import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { FormattedProvider } from 'react-native-globalize';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';

import Navigator from './Navigator';
import { getToken } from './screens/User/loginUtils';

const authLink = setContext(async (req, { headers }) => {
  const token = await getToken();

  return {
    ...headers,
    headers: {
      authorization: token ? `Bearer ${token}` : null
    }
  }
})

const httpLink = new HttpLink({
  uri: "https://api.graph.cool/simple/v1/cjka0xun5847i0160d4eflzpb"
})

const link = authLink.concat(httpLink);

// this is our connection to our graphcool server/db
const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
})

export default class App extends Component {

  render() {
    return (
      <FormattedProvider locale="en">
        <ApolloProvider client={client}>
          <Navigator />

        </ApolloProvider>
      </FormattedProvider>
    );
  }
}


