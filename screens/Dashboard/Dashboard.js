import React, { Component } from "react"
import { StyleSheet, ActivityIndicator, View } from "react-native"
import { Container, Content } from "native-base"
import { Query } from "react-apollo"

import theme from "../../styles/theme"
import FooterTabs from "../../layout/FooterTabs"
import GET_USER_DETAILS from "../../graphql/queries/GetUserDetails"
import FavPropertiesCard from "./FavPropertiesCard"
import ContactEventCard from "../ContactEvents/ContactEventCard"

export default class Dashboard extends Component {
	static navigationOptions = {
		title: "Dashboard"
	}

	render() {
		const { screenProps } = this.props
		return (
			<Container style={styles.container}>
				<Content>
					<Query query={GET_USER_DETAILS} variables={{ id: screenProps.user.id }}>
						{({ loading, error, data }) => {
							if (loading) return <ActivityIndicator size="large" style={styles.spinner} />
							if (error) return `Error! ${error}`

							const { favoriteProperties, events } = data.User

							let calls = []
							let emails = []
							let visits = []

							events.map(event => {
								switch (event.type) {
									case "CALL":
										calls.push(event)
										break
									case "EMAIL":
										emails.push(event)
										break
									case "VISIT":
										visits.push(event)
										break
								}
							})

							return (
								<View>
									<FavPropertiesCard title="Favorite Properties" items={favoriteProperties} navigation={this.props.navigation} />
									<ContactEventCard title="Recent Calls" items={calls} navigation={this.props.navigation} />
									<ContactEventCard title="Recent Emails" items={emails} navigation={this.props.navigation} />
									<ContactEventCard title="Recent Visits" items={visits} navigation={this.props.navigation} />
								</View>
							)
						}}
					</Query>
				</Content>

				<FooterTabs activeBtn="dashboard" navigation={this.props.navigation} />
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	spinner: {
		marginTop: "70%"
	}
})
