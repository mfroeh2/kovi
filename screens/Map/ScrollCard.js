import React, { Component } from "react"
import { View, Text, Dimensions, StyleSheet, ImageBackground, TouchableHighlight } from "react-native"
import { MailComposer } from "expo"
import { Icon } from "native-base"
import { Mutation } from "react-apollo"
import call from "react-native-phone-call"
import { showLocation } from "react-native-map-link"
import * as Animatable from "react-native-animatable"

import theme from "../../styles/theme"
import CREATE_CONTACT_EVENT from "../../graphql/mutations/CreateContactEvent"
import GET_USER_DETAILS from "../../graphql/queries/GetUserDetails"

const { height } = Dimensions.get("window")

const CARD_HEIGHT = height / 3.5
const CARD_WIDTH = height / 4 + 100

export default class ScrollCard extends Component {
	openMap = () => {
		const { property } = this.props
		showLocation({
			latitude: property["coordinates"]["latitude"],
			longitude: property["coordinates"]["longitude"],
			title: property["propertyName"],
			googleForceLatLon: false, // optionally force GoogleMaps to use the latlon for the query instead of the title
			dialogTitle: "Open in Maps", // optional (default: 'Open in Maps')
			dialogMessage: "Choose your preferred maps app", // optional (default: 'What app would you like to use?')
			cancelText: "Cancel" // optional (default: 'Cancel')
		})
	}

	call = () => {
		const args = {
			number: `${this.props.property["phone"]}`,
			prompt: true
		}

		call(args).catch(console.error)
	}

	composeEmail = recipient => {
		MailComposer.composeAsync({
			recipients: [recipient],
			subject: "",
			body: ""
		})
	}

	goToProperty = () => {
		const { property, index } = this.props
		this.props.navigation.navigate("Property", { id: property.id, index: index })
	}

	render() {
		const { property, userId } = this.props

		return (
			<Animatable.View useNativeDriver={true} animation="bounceInUp" style={styles.card} key={property.id}>
				<TouchableHighlight style={styles.imageContainer} onPress={this.goToProperty} activeOpacity={0.75} underlayColor={theme.COLOR_GREEN}>
					<ImageBackground style={styles.cardImage} resizeMode="cover" source={require("../../assets/images/apartment-placeholder-min.jpg")} />
				</TouchableHighlight>
				<View style={styles.cardBottom}>
					<TouchableHighlight style={styles.titleSection} onPress={this.goToProperty} activeOpacity={0.85} underlayColor={theme.COLOR_GREEN_OPACITY}>
						<Text numberOfLines={1} style={styles.cardtitle}>
							{property.propertyName}
						</Text>
					</TouchableHighlight>
					<View style={styles.cardRow}>
						<TouchableHighlight onPress={this.openMap} activeOpacity={0.75} underlayColor={theme.COLOR_GREEN}>
							<View>
								<Text numberOfLines={1} style={styles.cardDescription}>
									{property.address.street}
								</Text>
								<Text numberOfLines={1} style={styles.cardDescription}>
									{property.address.city}, {property.address.state} {property.address.zip}
								</Text>
							</View>
						</TouchableHighlight>
						<View style={styles.buttonRow}>
							<Mutation
								mutation={CREATE_CONTACT_EVENT}
								variables={{
									type: "CALL",
									propertyId: property.id,
									userId
								}}
								refetchQueries={() => [
									{
										query: GET_USER_DETAILS,
										variables: {
											id: userId
										}
									}
								]}
							>
								{(AddContactEvent, { data }) => {
									return (
										<TouchableHighlight
											style={styles.actionBtn}
											activeOpacity={1}
											underlayColor={"rgba(0,0,0,0.1)"}
											onPress={() => {
												AddContactEvent()
												this.call()
											}}
										>
											<Icon type="Entypo" name="phone" style={[styles.buttonIcon, styles.callIcon]} />
										</TouchableHighlight>
									)
								}}
							</Mutation>
							<Mutation
								mutation={CREATE_CONTACT_EVENT}
								variables={{
									type: "EMAIL",
									propertyId: property.id,
									userId
								}}
								refetchQueries={() => [
									{
										query: GET_USER_DETAILS,
										variables: {
											id: userId
										}
									}
								]}
							>
								{(AddContactEvent, { data }) => {
									return (
										<TouchableHighlight
											style={styles.actionBtn}
											activeOpacity={1}
											underlayColor={"rgba(0,0,0,0.1)"}
											onPress={() => {
												AddContactEvent()
												this.composeEmail(property.email)
											}}
										>
											<Icon type="Entypo" name="mail" style={[styles.buttonIcon, styles.emailIcon]} />
										</TouchableHighlight>
									)
								}}
							</Mutation>
						</View>
					</View>
				</View>
			</Animatable.View>
		)
	}
}

const styles = StyleSheet.flatten({
	card: {
		backgroundColor: "#fff",
		marginHorizontal: 10,
		height: CARD_HEIGHT,
		width: CARD_WIDTH,
		overflow: "hidden"
	},
	imageContainer: {
		height: 100,
		flex: 3
	},
	cardImage: {
		height: "100%",
		width: "100%"
	},
	cardBottom: {
		paddingLeft: 10,
		paddingRight: 10,
		flex: 2,
		justifyContent: "space-between"
	},
	cardRow: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		flex: 2,
		paddingLeft: 10,
		paddingRight: 10
	},
	titleSection: {
		flex: 1,
		justifyContent: "center",
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 3,
		paddingBottom: 3,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: "rgba(0,0,0,0.2)"
	},
	cardtitle: {
		fontSize: 16,
		marginTop: 5,
		fontWeight: "bold"
	},
	cardDescription: {
		fontSize: 12,
		color: "#444"
	},
	buttonRow: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	actionBtn: {
		padding: 3,
		marginHorizontal: 5,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 3
		},
		shadowOpacity: 0.27,
		shadowRadius: 4.65,
		elevation: 10,
		borderRadius: 50
	},
	buttonIcon: {
		fontSize: 24,
		padding: 0,
		margin: 0
	},
	callIcon: {
		color: theme.COLOR_GREEN
	},
	emailIcon: {
		color: theme.COLOR_ORANGE
	}
})
