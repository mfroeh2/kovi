import gql from 'graphql-tag';

const ADD_FAVORITE = gql`
    mutation AddFavorite($userId: ID!, $propertyId: ID!) {
        createFavoriteProperty(userId: $userId, propertyId: $propertyId) {
            id
        }
    }
`;

export default ADD_FAVORITE;