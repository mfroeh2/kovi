import gql from 'graphql-tag';

const PROPERTY_DETAILS = gql`
    query Property($id: ID!) {
        Property(id: $id) {
            id
            propertyName
            units
            phone
            email
            onSiteMgr
            regionalMgr
            maintenanceSupervisor
            mgmtCo {
                name
                id
            }
            coordinates {
                latitude
                longitude
            }
            ownershipCo
            address {
                street
                city
                state
                zip
            }
        }
    }
`;

export default PROPERTY_DETAILS