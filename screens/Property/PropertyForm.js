import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Form, Item, Input, Label, Button } from 'native-base';

import theme from '../../styles/theme';

export default class PropertyForm extends Component {
    static defaultProps = {
        property: {}
    }

    state = {
        propertyName: this.props.property.propertyName || "",
        address: this.props.property.address || ""
    };

    submitForm = () => {
        this.props.onSubmit({
            propertyName: this.state.propertyName,
            address: this.state.address
        });
    }

    render() {
        return (
            <Form style={styles.form}>
                <Item floatingLabel>
                    <Label>Property Name</Label>

                    <Input
                        onChangeText={propertyName => this.setState({propertyName})}
                        value={this.state.propertyName}
                    />
                </Item>
                <Item floatingLabel>
                    <Label>Address</Label>

                    <Input
                        onChangeText={address => this.setState({address})}
                        value={this.state.address}
                    />
                </Item>
                <Button block onPress={this.submitForm} style={styles.button}>
                    <Text style={styles.buttonText}>Save Property</Text>
                </Button>
            </Form>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: theme.COLOR_ORANGE
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold'
    }
})