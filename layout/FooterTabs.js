import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Footer, FooterTab, Icon, Button } from 'native-base';

import { withNavigation } from 'react-navigation';
import theme from '../styles/theme';

class FooterTabs extends Component {

  goToDash = () => {
    this.props.navigation.navigate('Dashboard');
}

  goToMap = () => {
    this.props.navigation.navigate('KoviMap');
  }

  goToProperties = () => {
    this.props.navigation.navigate('PropertiesList');
  }

  render() {
    const { activeBtn } = this.props;

    return (
      <Footer style={styles.footer}>

        <FooterTab>
          <Button
            vertical
            onPress={this.goToProperties}
            style={activeBtn === 'properties' ? styles.activeBtn : styles.footerBtn}
          >
            <Icon style={activeBtn === 'properties' ? styles.activeIcon : styles.footerIcon}
            type="Entypo" name="list" />
            <Text style={styles.footerTabText}>Properties</Text>
          </Button>
        </FooterTab>

        <FooterTab>
          <Button
            vertical
            onPress={this.goToMap}
            style={activeBtn === 'map' ? styles.activeBtn : styles.footerBtn}
          >
            <Icon style={activeBtn === 'map' ? styles.activeIcon : styles.footerIcon} name="map" />
            <Text style={styles.footerTabText}>Map</Text>
          </Button>
        </FooterTab>

        <FooterTab>
          <Button
            vertical
            onPress={this.goToDash}
            style={activeBtn === 'dashboard' ? styles.activeBtn : styles.footerBtn}
          >
            <Icon style={activeBtn === 'dashboard' ? styles.activeIcon : styles.footerIcon} name="speedometer" />
            <Text style={styles.footerTabText}>Dashboard</Text>
          </Button>
        </FooterTab>

      </Footer>
    );
  }
}

const styles = StyleSheet.create({
  footer: {
      backgroundColor: theme.COLOR_GREY
  },
  activeBtn: {
    backgroundColor: theme.COLOR_GREEN,
    height: '100%',
    width: '100%',
    borderRadius: 0
  },
  footerBtn: {
    height: '100%',
    width: '100%'
  },
  activeIcon: {
    color: '#fff'
  },
  footerIcon: {
    color: '#1dd3b0',
  },
  footerTabText: {
    color: '#fff',
  }
})

export default withNavigation(FooterTabs);