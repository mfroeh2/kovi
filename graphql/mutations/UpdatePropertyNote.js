import gql from 'graphql-tag';

const UPDATE_PROPERTY_NOTE = gql`
  mutation updatePropertyNote( $id: ID!, $title: String!, $body: String ) {
    updatePropertyNote(id: $id, title: $title, body: $body) {
      id
    }
  }
`;

export default UPDATE_PROPERTY_NOTE