import React, { Component } from "react"
import { ActivityIndicator } from "react-native"
import { Container, Content } from "native-base"

import { graphql } from "react-apollo"
import gql from "graphql-tag"

import PropertyForm from "./PropertyForm"
import FooterTabs from "../../layout/FooterTabs"

class NewProperty extends Component {
	state = {
		loading: false
	}

	newProperty = ({ propertyName, address }) => {
		const { newProperty, navigation, screenProps } = this.props
		this.setState({ loading: true })
		newProperty({
			variables: {
				propertyName,
				address,
				userId: screenProps.user.id
			}
		})
			.then(() => {
				navigation.goBack()
			})
			.catch(error => {
				this.setState({ loading: false })
				console.log(error)
			})
	}

	render() {
		return (
			<Container>
				<Content>{this.state.loading ? <ActivityIndicator size="large" /> : <PropertyForm onSubmit={this.newProperty} />}</Content>

				<FooterTabs activeBtn="properties" navigation={this.props.navigation} />
			</Container>
		)
	}
}

const newProperty = gql`
	mutation newProperty($propertyName: String!, $address: String!, $userId: ID!) {
		createProperty(propertyName: $propertyName, address: $address, userId: $userId) {
			id
		}
	}
`

export default graphql(newProperty, {
	name: "newProperty",
	options: {
		refetchQueries: ["getProperties"]
	}
})(NewProperty)
