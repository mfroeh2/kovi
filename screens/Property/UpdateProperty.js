import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native';
import { Container, Content} from 'native-base';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';


import PropertyForm from './PropertyForm';
import FooterTabs from '../../layout/FooterTabs';




class UpdateProperty extends Component {
  state = {
    loading: false
  };

  updateProperty = ({ propertyName, address }) => {
    const { updateProperty, navigation, screenProps } = this.props;
    this.setState({ loading: true });
    updateProperty({
      variables: {
        id: this.props.Property.id,
        propertyName,
        address,
        userId: screenProps.user.id
      }
    })
      .then(() => {
        navigation.goBack();
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error);
      });
  };

  render() {
    return (
        <Container>

          <Content>
            {
                this.state.loading ? (
                    <ActivityIndicator size="large" />
                ) : (
                     <PropertyForm onSubmit={this.updateProperty} property={this.props.Property} />
                )
            }
          </Content>

            <FooterTabs
              activeBtn='properties'
              navigation={this.props.navigation}
            />

        </Container>
    )
  }
}

const propertyQuery = gql`
  query Property($id: ID!) {
    Property(id: $id) {
      id
      propertyName
      units
      address
      city
      state
      zip
      phone
      fax
      email
      onSiteMgr
      regionalMgr
      maintenanceSupervisor
      mgmtCo
      ownershipCo
    }
  }
`;

const updateProperty = gql`
  mutation updateProperty($id: ID!, $propertyName: String!, $address: String, $userId: ID!) {
    updateProperty(id: $id, propertyName: $propertyName, address: $address, userId: $userId) {
      id
    }
  }
`;

export default compose(
  graphql(updateProperty, {
    name: 'updateProperty',
    options: {
      refetchQueries: ["Property"]
    }
  }),
  graphql(propertyQuery, {
    props: ({ data }) => ({ ...data }),
    options: ({ navigation }) => ({
        variables: {
            id: navigation.state.params.id
        }
    })
  })
)(UpdateProperty);
