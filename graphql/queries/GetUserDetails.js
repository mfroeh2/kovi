import gql from 'graphql-tag';

const GET_USER_DETAILS = gql`
    query UserDetails($id: ID!) {
        User(id: $id) {
            id
            events {
                type
                id
                createdAt
                property {
                    propertyName
                    id
                }
            }
            favoriteProperties {
                property {
                    propertyName
                    id
                }
                createdAt
                id
            }
        }
    }
`;

export default GET_USER_DETAILS;