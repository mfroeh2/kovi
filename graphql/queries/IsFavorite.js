import gql from 'graphql-tag';

const IS_FAVORITE = gql`
    query User($id: ID!, $propertyId: ID!){
        User(id: $id) {
            favoriteProperties(filter: {
                property: {
                    id: $propertyId
                }
            }) {
                id
            }
            id
        }
    }
`;

export default IS_FAVORITE;

