import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet, TouchableHighlight } from 'react-native'
import { Container, Content, List, Body, ListItem, Right, Left } from 'native-base'
import { FormattedDate } from 'react-native-globalize';

import FooterTabs from '../../layout/FooterTabs';

import FavoritePropertyListItem from '../Dashboard/FavoritePropertyListItem'


export default class ContactEventPage extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.state.params.type
        }

    }

    goToProperty = (id, propertyName) => {
        props.navigation.navigate('Property', {
            id: id,
            propertyName: propertyName
        })
    }

    render() {
        const { items } = this.props.navigation.state.params;

        const { navigation } = this.props;

        return (
        <Container>

            <Content>
                <List>
                    <FlatList
                        data={items}
                        renderItem={({item}) => (
                            <FavoritePropertyListItem item={item} navigation={navigation} />
                        )}
                        keyExtractor={item => item.id}
                    />
                </List>
            </Content>
            <FooterTabs
                activeBtn='dashboard'
                navigation={navigation}
            />
        </Container>
        )
    }


}

