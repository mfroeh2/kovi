import React, { Component } from "react"

import ActionButton from "./ActionButton"

export default class Nearby extends Component {
	goToMap = () => {
		this.props.navigation.navigate("KoviMap", {
			coords: {
				latitude: this.props.coords.latitude,
				longitude: this.props.coords.longitude
			}
		})
	}

	render() {
		return (
			<ActionButton icon="location" onPress={this.goToMap}>
				Nearby
			</ActionButton>
		)
	}
}
