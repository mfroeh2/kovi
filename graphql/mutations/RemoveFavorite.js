import gql from 'graphql-tag';

const REMOVE_FAVORITE = gql`
    mutation RemoveFavorite($id: ID!) {
        deleteFavoriteProperty(id: $id) {
            id
        }
    }
`;

export default REMOVE_FAVORITE;