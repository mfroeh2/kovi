import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content } from 'native-base';

import FooterTabs from '../../layout/FooterTabs';
import theme from '../../styles/theme';
import Properties from './Properties';

export default class PropertiesList extends Component {

    static navigationOptions = {
        title: 'Properties'
    }

    render() {

        const { user } = this.props.screenProps;

        return (
          <Container style={styles.container}>

            <Content>
                <Properties {...this.props}/>
            </Content>

                <FooterTabs
                    activeBtn='properties'
                    navigation={this.props.navigation}
                />

          </Container>
      );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between'
    },
    footer: {
        backgroundColor: theme.COLOR_GREY
    },

})