import React, { Component } from "react"
import { View } from "react-native"
import Modal from "react-native-modal"
import { graphql } from "react-apollo"
import gql from "graphql-tag"

import NoteForm from "../../components/NoteForm"

class UpdatePropertyNote extends Component {
	updatePropertyNote = ({ title, body }) => {
		const { updatePropertyNote, toggleModal, note } = this.props
		updatePropertyNote({
			variables: {
				id: note.id,
				title,
				body
			}
		})
			.then(() => {
				toggleModal()
			})
			.catch(error => {
				console.log(error)
			})
	}

	render() {
		const { toggleModal, note, visible } = this.props

		return (
			<View>
				<Modal animationType="slide" transparent={true} isVisible={visible} avoidKeyboard={true}>
					<NoteForm toggleModal={toggleModal} onSubmit={this.updatePropertyNote} note={note} />
				</Modal>
			</View>
		)
	}
}

const updatePropertyNote = gql`
	mutation updatePropertyNote($id: ID!, $title: String!, $body: String) {
		updatePropertyNote(id: $id, title: $title, body: $body) {
			id
		}
	}
`

export default graphql(updatePropertyNote, {
	name: "updatePropertyNote",
	options: {
		refetchQueries: ["getNotes"]
	}
})(UpdatePropertyNote)
