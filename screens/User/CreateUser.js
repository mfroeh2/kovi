import React, { Component } from "react"
import { Text, View, StyleSheet } from "react-native"
import { H2 } from "native-base"
import { graphql, compose } from "react-apollo"
import gql from "graphql-tag"

import UserForm from "./UserForm"
import { signIn } from "./loginUtils"

class CreateUser extends Component {
	createUser = async ({ email, password }) => {
		this.props.onSubmit()
		try {
			const user = await this.props.createUser({
				variables: { email, password }
			})
			const signin = await this.props.signinUser({
				variables: { email, password }
			})
			signIn(signin.data.signinUser.token)
			this.props.client.resetStore()
		} catch (e) {
			console.log(e)
		}
	}

	render() {
		return (
			<View>
				<H2 style={styles.text}>Register</H2>
				<UserForm onSubmit={this.createUser} type="Register" />
			</View>
		)
	}
}

const createUser = gql`
	mutation createUser($email: String!, $password: String!) {
		createUser(authProvider: { email: { email: $email, password: $password } }) {
			id
		}
	}
`

const signinUser = gql`
	mutation signinUser($email: String!, $password: String!) {
		signinUser(email: { email: $email, password: $password }) {
			token
		}
	}
`

const styles = StyleSheet.create({
	text: {
		marginLeft: 20,
		textAlign: "center",
		color: "#fff",
		fontWeight: "100",
		fontSize: 45,
		lineHeight: 45
	}
})

export default compose(
	graphql(signinUser, { name: "signinUser" }),
	graphql(createUser, { name: "createUser" })
)(CreateUser)
