import React, { Component } from "react"
import { Text, View, StyleSheet } from "react-native"
import { Button, Icon } from "native-base"

import theme from "../../styles/theme"

export default class ActionButton extends Component {
	handlePress = e => {
		e.preventDefault()
		this.props.onPress()
	}

	render() {
		const { children, icon } = this.props

		return (
			<Button style={styles.actionBtn} onPress={this.handlePress}>
				<Icon name={icon} type="Entypo" style={styles.actionBtnIcon} />
				<Text style={styles.actionBtnText}>{children}</Text>
			</Button>
		)
	}
}

const styles = StyleSheet.create({
	actionBtn: {
		backgroundColor: theme.COLOR_ORANGE,
		width: "24%",
		flexDirection: "column",
		height: 55
	},
	actionBtnText: {
		color: "#fff",
		fontWeight: "500",
		marginTop: 0
	},
	actionBtnIcon: {
		fontSize: 20
	}
})
