import React, { Component } from "react"
import { View, ActivityIndicator, StyleSheet } from "react-native"
import { Query } from "react-apollo"
import gql from "graphql-tag"

import PropertiesList from "../../components/PropertiesList"

export default class Properties extends Component {
	render() {
		const { navigation } = this.props
		return (
			<View>
				<Query query={GET_PROPERTIES}>
					{({ loading, error, data }) => {
						if (loading) return <ActivityIndicator size="large" style={styles.spinner} />
						if (error) return console.log(error)

						return <PropertiesList data={data.allProperties} navigation={navigation} />
					}}
				</Query>
			</View>
		)
	}
}

// this is our call to the api to get the data and pass it to this component as this.props.data
const GET_PROPERTIES = gql`
	query getProperties {
		allProperties {
			id
			propertyName
		}
	}
`

const styles = StyleSheet.create({
	listItem: {
		marginLeft: 0,
		paddingLeft: 15
	},
	spinner: {
		marginTop: "70%"
	}
})
