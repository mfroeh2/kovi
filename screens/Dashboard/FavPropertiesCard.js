import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableHighlight } from 'react-native';
import { Card, CardItem, Left, Body, Icon, H3, ListItem, Right } from 'native-base';

import theme from '../../styles/theme';
import PropertiesList from '../../components/PropertiesList';

const FavPropertiesCard = (props) => {

    const favProperties = props.items.map(item => {
        return item.property;
    })

    goToFavoritePropertiesPage = (items) => {
        props.navigation.navigate('FavoritePropertiesPage', {
            items: items
        });
    }

    const lastItems = favProperties.reverse().slice(0,5);
    const showing = lastItems.length < 5 ? "All" : "Last 5";

    return (
      <Card style={styles.card}>
          <CardItem style={styles.cardTitle}>
              <Left>
                  <Icon type="Entypo" name='star' style={styles.icon} />
                  <Body>
                    <H3 style={styles.title}>{props.title}</H3>
                    <Text style={styles.small}>- Showing {showing} -</Text>
                  </Body>
              </Left>
          </CardItem>
          <CardItem style={styles.cardBody}>

            <PropertiesList data={lastItems} navigation={props.navigation} />

          </CardItem>
          <CardItem style={styles.cardBottom}>
              <Left>
                  <Text>Total: {props.items.length}</Text>
              </Left>
              <Body />
              <Right>
                  <TouchableHighlight onPress={() => this.goToFavoritePropertiesPage(favProperties)}>
                    <Text style={styles.link}>See All</Text>
                  </TouchableHighlight>
              </Right>
          </CardItem>
      </Card>
    );
}

const styles = StyleSheet.create({
    text: {
        color: theme.COLOR_GREY
    },
    card: {
        width: '90%',
        marginRight: '5%',
        marginLeft: '5%',
        marginTop: 20
    },
    cardTitle: {
        backgroundColor: '#ededed',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: 'rgba(0,0,0,0.2)'
    },
    title: {
        color: theme.COLOR_GREY
    },
    cardBody: {
        backgroundColor: '#ededed',
    },
    icon: {
        color: theme.COLOR_GREEN
    },
    cardBottom: {
        backgroundColor: '#ededed',
    },
    link: {
        textDecorationLine: 'underline',
        color: theme.COLOR_GREY,
    },
    small: {
        fontSize: 10,
        color: 'rgba(0,0,0,0.3)',
    },
})

export default FavPropertiesCard;