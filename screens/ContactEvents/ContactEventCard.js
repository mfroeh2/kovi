import React from 'react';
import { Text, FlatList, StyleSheet, TouchableHighlight } from 'react-native';
import { Card, CardItem, Left, Body, Icon, H3, Right } from 'native-base';

import theme from '../../styles/theme';
import ContactListItem from '../Dashboard/ContactListItem'

const ContactEventCard = (props) => {

    goToContactPage = (type, items) => {
        props.navigation.navigate('ContactEventPage',
        {
            type: type,
            items: items
        });
    };

    let icon = '';
    let type = '';

    switch(props.title) {
        case 'Recent Calls':
            icon = 'phone'
            type = 'Calls'
            break;
        case 'Recent Emails':
            icon = 'email'
            type = 'Emails'
            break;
        case 'Recent Visits':
            icon = 'location'
            type = 'Visits'
            break;
        default:
            icon = 'leaf'
    }

    const lastItems = props.items.reverse().slice(0,5);
    const showing = lastItems.length < 5 ? "All" : "Last 5";

    return (
      <Card style={styles.card}>
          <CardItem style={styles.cardTitle}>
              <Left>
                  <Icon type="Entypo" name={icon} style={styles.icon} />
                  <Body>
                    <H3 style={styles.title}>{props.title}</H3>
                    <Text style={styles.small}>- Showing {showing} -</Text>
                  </Body>
              </Left>
          </CardItem>
          <CardItem style={styles.cardBody}>

              <FlatList
                    data={lastItems}
                    style={styles.list}
                    renderItem={({item}) => (
                        <ContactListItem item={item} navigation={props.navigation} />
                    )}
                    keyExtractor={item => item.id}
                />

          </CardItem>
          <CardItem style={styles.cardBottom}>
              <Left>
                  <Text>Total: {props.items.length}</Text>
              </Left>
              <Body />
              <Right>
                  <TouchableHighlight onPress={() => this.goToContactPage(type, props.items)}>
                    <Text style={styles.link}>See All</Text>
                  </TouchableHighlight>
              </Right>
          </CardItem>
      </Card>
    );
}

const styles = StyleSheet.create({
    card: {
        width: '90%',
        marginRight: '5%',
        marginLeft: '5%',
        marginTop: 20
    },
    cardTitle: {
        backgroundColor: '#ededed',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: 'rgba(0,0,0,0.2)'
    },
    title: {
        color: theme.COLOR_GREY
    },
    cardBody: {
        backgroundColor: '#ededed',
    },
    list: {
        borderRadius: 5
    },
    icon: {
        color: theme.COLOR_GREEN
    },
    cardBottom: {
        backgroundColor: '#ededed',
    },
    link: {
        textDecorationLine: 'underline',
        color: theme.COLOR_GREY,
    },
    small: {
        fontSize: 10,
        color: 'rgba(0,0,0,0.3)',
    },
})

export default ContactEventCard;