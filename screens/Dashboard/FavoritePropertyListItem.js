import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { ListItem, Left, Body, Right, Icon } from 'native-base'

import theme from '../../styles/theme';


const FavoritePropertyListItem = (props) => {

    const { item } = props;

    return (
        <ListItem
            style={styles.listItem}
            onPress={() => props.navigation.navigate('Property', {
                id: item.id,
                propertyName: item.propertyName
            })}
        >
            <Left>
                <Text style={styles.text}>{item.propertyName}</Text>
            </Left>
            <Body>

            </Body>
            <Right>
                <Icon name="arrow-forward" />
            </Right>
        </ListItem>
    )
}


const styles = StyleSheet.create({
    text: {
        color: theme.COLOR_GREY
    },
    list: {
        width: '100%'
    },
    listItem: {
        marginLeft: 0,
        paddingLeft: 15,
        backgroundColor: '#fff',
        width: '100%',
    }
})

export default FavoritePropertyListItem;