import React, { Component } from "react"
import { StyleSheet, Animated, Dimensions, FlatList, Easing, View, StatusBar } from "react-native"
import { Container, Button, Icon } from "native-base"
import MapView, { Marker } from "react-native-maps"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import * as Animatable from "react-native-animatable"

import ScrollCard from "./ScrollCard"
import TransparentHeader from "../../layout/TransparentHeader"
import FooterTabs from "../../layout/FooterTabs"
import theme from "../../styles/theme"

const { width, height } = Dimensions.get("window")

const CARD_HEIGHT = height / 4
const CARD_WIDTH = CARD_HEIGHT + 100
const CARD_WITH_MARGIN = CARD_WIDTH + 20

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList)

class KoviMap extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			headerStyle: {
				backgroundColor: "transparent",
				borderBottomWidth: 0
			},
			header: props => <TransparentHeader {...props} />,
			headerTintColor: theme.COLOR_GREEN,
			headerRight: (
				<Button transparent onPress={() => navigation.navigate("User")}>
					<Icon style={{ color: theme.COLOR_GREEN }} type="Entypo" name="user" />
				</Button>
			)
		}
	}

	state = {
		initialLatitude: 35.2271,
		initialLongitude: -80.8431,
		latitudeDelta: 0.0922,
		longitudeDelta: 0.421,
		scrollCardAnim: new Animated.Value((CARD_HEIGHT + 50) * -1),
		scrollToggleAnim: new Animated.Value(-40)
	}

	componentWillMount() {
		this.index = 0
		this.animation = new Animated.Value(0)

		let coordinates = {
			latitude: this.state.initialLatitude,
			longitude: this.state.initialLongitude
		}

		clearTimeout(this.nearbyProperties)
		this.nearbyProperties = setTimeout(() => {
			this.map.animateToRegion(
				{
					...coordinates,
					latitudeDelta: this.state.latitudeDelta,
					longitudeDelta: this.state.longitudeDelta
				},
				350
			)
		}, 10)
	}

	componentDidMount() {
		this.props.navigation.setParams({
			goToProperties: this.goToProperties,
			goToDash: this.goToDash
		})

		this.animation.addListener(({ value }) => {
			let index = Math.floor(value / CARD_WIDTH + 0.3)
			if (index >= this.props.allProperties.length) {
				index = this.props.allProperties.length - 1
			}
			if (index <= 0) {
				index = 0
			}

			clearTimeout(this.regionTimeout)
			this.regionTimeout = setTimeout(() => {
				if (this.index !== index) {
					this.index = index
					const { coordinates, id } = this.props.allProperties[index]
					this.map.animateToRegion(
						{
							...coordinates,
							latitudeDelta: this.state.latitudeDelta,
							longitudeDelta: this.state.longitudeDelta
						},
						350
					)
				}
			}, 10)
		})
	}

	showScrollCards = () => {
		Animated.sequence([
			Animated.timing(this.state.scrollCardAnim, {
				toValue: 20,
				duration: 300,
				easing: Easing.elastic()
			}),
			Animated.timing(this.state.scrollToggleAnim, {
				toValue: 0,
				duration: 200,
				easing: Easing.elastic()
			})
		]).start()
	}

	hideScrollCards = () => {
		Animated.sequence([
			Animated.timing(this.state.scrollCardAnim, {
				toValue: -300,
				duration: 300,
				easing: Easing.back()
			}),
			Animated.timing(this.state.scrollToggleAnim, {
				toValue: -40,
				duration: 200,
				easing: Easing.back()
			})
		]).start()
	}

	_renderItem = (item, index) => {
		return <ScrollCard property={item} navigation={this.props.navigation} userId={this.props.screenProps.user.id} index={index} />
	}

	scrollToCard = index => {
		this.mapScroll.getNode().scrollToIndex({ index })
	}

	render() {
		const { allProperties } = this.props

		return (
			<Container>
				<StatusBar barStyle="dark-content" />
				<MapView
					ref={map => (this.map = map)}
					style={styles.map}
					initialRegion={{
						latitude: this.state.initialLatitude,
						longitude: this.state.initialLongitude,
						latitudeDelta: 0.0922,
						longitudeDelta: 0.421
					}}
				>
					{allProperties &&
						allProperties.map((property, index) => (
							<Marker
								coordinate={property.coordinates}
								title={property.propertyName}
								pinColor={theme.COLOR_GREEN}
								key={index}
								onPress={() => {
									this.scrollToCard(index)
									this.showScrollCards()
								}}
							/>
						))}
				</MapView>

				{allProperties && (
					<View style={styles.scrollCards}>
						<AnimatedFlatList
							ref={ref => (this.mapScroll = ref)}
							data={allProperties}
							getItemLayout={(data, index) => ({ length: CARD_WITH_MARGIN, offset: CARD_WITH_MARGIN * index, index })}
							initialNumToRender={2}
							horizontal
							scrollEventThrottle={16}
							initialScrollIndex={0}
							showsHorizontalScrollIndicator={false}
							snapToInterval={CARD_WITH_MARGIN}
							onScroll={Animated.event(
								[
									{
										nativeEvent: {
											contentOffset: {
												x: this.animation
											}
										}
									}
								],
								{ useNativeDriver: true }
							)}
							style={[styles.scrollView, { bottom: this.state.scrollCardAnim }]}
							contentContainerStyle={styles.scrollContainer}
							scrollEventThrottle={16}
							renderItem={({ item, index }) => this._renderItem(item, index)}
							keyExtractor={item => item.id}
						/>
						<Animated.View style={[styles.downArrow, { bottom: this.state.scrollToggleAnim }]}>
							<Icon type="Entypo" name="chevron-thin-down" onPress={this.hideScrollCards} />
						</Animated.View>
					</View>
				)}

				<FooterTabs activeBtn="map" navigation={this.props.navigation} />
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	header: {
		backgroundColor: "#fff",
		borderBottomWidth: 0,
		width: "100%",
		position: "absolute",
		zIndex: 10
	},
	headerTitle: {
		color: theme.COLOR_GREEN,
		fontSize: 20
	},
	headerIcon: {
		color: theme.COLOR_GREEN
	},
	map: {
		flex: 1,
		zIndex: -1,
		height: "110%"
	},
	scrollView: {
		position: "absolute",
		left: 0,
		right: 0,
		paddingVertical: 10
	},
	scrollContainer: {
		paddingRight: (width - CARD_WIDTH - 20) / 2,
		paddingLeft: (width - CARD_WIDTH - 20) / 2,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 3
		},
		shadowOpacity: 0.27,
		shadowRadius: 4.65,
		elevation: 20
	},
	scrollCards: {
		backgroundColor: "transparent",
		alignItems: "center"
	},
	downArrow: {
		position: "absolute"
	}
})

const getMarkers = gql`
	query getMarkers {
		allProperties {
			id
			propertyName
			units
			phone
			email
			address {
				street
				city
				state
				zip
			}
			coordinates {
				latitude
				longitude
			}
		}
	}
`

export default graphql(getMarkers, {
	props: ({ data }) => ({ ...data })
})(KoviMap)
