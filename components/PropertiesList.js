import React from 'react';
import { FlatList, StyleSheet } from 'react-native';
import { List } from 'native-base';

import PropertiesListItem from './PropertiesListItem';

const PropertiesList = ({data, navigation}) => {

    return (
        <List style={styles.list}>
            <FlatList
                data={data}
                renderItem={({item}) => <PropertiesListItem item={item} navigation={navigation} /> }
                keyExtractor={item => item.id}
            />
        </List>
    );
}

const styles = StyleSheet.create({
    list: {
        width: '100%'
    }
})

export default PropertiesList;