import React, { Component } from "react"
import { View, Text, StyleSheet, Alert } from "react-native"
import { Container, Content, H3, Button, Accordion, Icon, Fab } from "native-base"

import { FormattedDate } from "react-native-globalize"
import { Mutation } from "react-apollo"

import FooterTabs from "../../layout/FooterTabs"
import NewPropertyNote from "./NewPropertyNote"
import UpdatePropertyNote from "./UpdatePropertyNote"

import DELETE_PROPERTY_NOTE from "../../graphql/mutations/DeletePropertyNote"
import theme from "../../styles/theme"

export default class PropertyNotes extends Component {
	static navigationOptions = {
		title: "Notes"
	}

	state = {
		newModalVisible: false,
		updateModalVisible: false,
		activeNote: {},
		loading: false
	}

	_renderContent = ({ title, body, id }) => {
		return (
			<View style={styles.accordionContent}>
				<Text>{body}</Text>
				<View style={styles.buttonRow}>
					<Button
						block
						onPress={() => {
							this.setState({
								activeNote: {
									title: title,
									body: body,
									id: id
								}
							})
							this.toggleUpdateModal()
						}}
						style={[styles.circleButton, styles.updateButton]}
					>
						<Icon type="Entypo" name="edit" style={styles.updateIcon} />
					</Button>
					<Mutation mutation={DELETE_PROPERTY_NOTE} refetchQueries={["getNotes"]}>
						{(deletePropertyNote, { data }) => (
							<Button
								block
								onPress={() => {
									this.setState({
										activeNote: {
											title: title,
											body: body,
											id: id
										},
										loading: true
									})
									Alert.alert(
										"Are You Sure?",
										"All unsaved changes will be lost...",
										[
											{
												text: "Delete",
												onPress: () =>
													deletePropertyNote({
														variables: {
															id: this.state.activeNote.id
														},
														refetchQueries: ["getNotes"]
													})
											},
											{ text: "Cancel", onPress: () => "" }
										],
										{ cancelable: false }
									)
								}}
								style={[styles.circleButton, styles.deleteButton]}
							>
								<Icon type="Entypo" name="trash" style={styles.updateIcon} />
							</Button>
						)}
					</Mutation>
				</View>
			</View>
		)
	}

	_renderHeader = ({ title, createdAt }, expanded) => {
		return (
			<View style={styles.accordionHeader}>
				<Text>{title}</Text>
				<View style={styles.accordionHeaderRight}>
					<FormattedDate value={new Date(createdAt)} style={styles.datetime} date="short" />
					{expanded ? <Icon type="Entypo" name="chevron-small-up" /> : <Icon type="Entypo" name="chevron-small-down" />}
				</View>
			</View>
		)
	}

	toggleNewModal = () => {
		this.setState({ newModalVisible: !this.state.newModalVisible })
	}

	toggleUpdateModal = () => {
		this.setState({ updateModalVisible: !this.state.updateModalVisible })
	}

	render() {
		const { screenProps, navigation } = this.props
		const { newModalVisible, updateModalVisible } = this.state
		const { allPropertyNotes, propertyName, id } = navigation.state.params

		const propertyNotes = allPropertyNotes.map(note => {
			return {
				title: note.title,
				body: note.body,
				id: note.id,
				createdAt: note.createdAt
			}
		})

		return (
			<Container>
				<Content style={styles.content}>
					<H3 style={styles.propertyTitle}>{propertyName}</H3>

					<View style={styles.mainContent}>
						{!propertyNotes.length && <Text>No notes yet, add one below!</Text>}

						<Accordion dataArray={propertyNotes} expanded={0} renderContent={this._renderContent} renderHeader={this._renderHeader} />

						<Button block iconLeft onPress={this.toggleNewModal} style={styles.button}>
							<Icon type="Entypo" name="plus" />
							<Text style={styles.buttonText}>New Note</Text>
						</Button>
					</View>

					<NewPropertyNote toggleModal={this.toggleNewModal} propertyId={id} user={screenProps.user} visible={newModalVisible} />

					<UpdatePropertyNote
						toggleModal={this.toggleUpdateModal}
						propertyId={id}
						user={screenProps.user}
						note={this.state.activeNote}
						visible={updateModalVisible}
					/>
				</Content>

				<FooterTabs activeBtn="properties" navigation={navigation} />
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	content: {
		padding: "2%"
	},
	mainContent: {
		alignContent: "space-between"
	},
	propertyTitle: {
		textAlign: "center",
		marginTop: 15,
		marginBottom: 15,
		color: theme.COLOR_GREY
	},
	button: {
		marginTop: 20,
		backgroundColor: theme.COLOR_ORANGE
	},
	buttonRow: {
		flexDirection: "row",
		justifyContent: "flex-end"
	},
	circleButton: {
		width: 40,
		height: 40,
		padding: 0,
		borderRadius: 100,
		marginTop: 10,
		marginBottom: 10,
		marginLeft: 5,
		marginRight: 5,
		alignSelf: "flex-end",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 3
		},
		shadowOpacity: 0.27,
		shadowRadius: 4.65,
		elevation: 10
	},
	updateButton: {
		backgroundColor: theme.COLOR_GREEN
	},
	deleteButton: {
		backgroundColor: theme.COLOR_ORANGE
	},
	updateIcon: {
		marginLeft: 0,
		marginRight: 0,
		fontSize: 20
	},
	buttonText: {
		color: "#fff",
		fontWeight: "bold"
	},
	accordionContent: {
		padding: 10
	},
	accordionHeader: {
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 5,
		paddingBottom: 5,
		backgroundColor: "rgba(68, 229, 178, 0.3)",
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: "#fff",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	accordionHeaderRight: {
		flexDirection: "row",
		alignItems: "center"
	},
	datetime: {
		fontSize: 12,
		color: "rgba(51,55,69,0.5)"
	}
})
