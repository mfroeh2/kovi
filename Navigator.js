import React, { Component } from "react"
import { ActivityIndicator, StyleSheet } from "react-native"
import { Query } from "react-apollo"
import { Button, Icon } from "native-base"

import USER_SIGNED_IN_QUERY from "./graphql/queries/UserSignedIn"

import { createStackNavigator } from "react-navigation"
import Property from "./screens/Property/Property"
import UpdateProperty from "./screens/Property/UpdateProperty"
import User from "./screens/User/User"
import Dashboard from "./screens/Dashboard/Dashboard"
import Login from "./screens/User/Login"
import KoviMap from "./screens/Map/KoviMap"
import PropertyNotes from "./screens/PropertyNotes/PropertyNotes"
import PropertiesList from "./screens/PropertiesList/PropertiesList"
import MgmtCompany from "./screens/MgmtCompany/MgmtCompany"
import ContactEventPage from "./screens/ContactEvents/ContactEventPage"
import FavoritePropertiesPage from "./screens/FavoriteProperties/FavoritePropertiesPage"
import theme from "./styles/theme"

GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest

const Navigator = createStackNavigator(
	{
		PropertiesList: {
			screen: PropertiesList
		},
		Property: {
			screen: Property
		},
		Dashboard: {
			screen: Dashboard
		},
		User: {
			screen: User
		},
		UpdateProperty: {
			screen: UpdateProperty
		},
		PropertyNotes: {
			screen: PropertyNotes
		},
		KoviMap: {
			screen: KoviMap
		},
		MgmtCompany: {
			screen: MgmtCompany
		},
		ContactEventPage: {
			screen: ContactEventPage
		},
		FavoritePropertiesPage: {
			screen: FavoritePropertiesPage
		}
	},
	{
		index: 0,
		initialRouteName: "KoviMap",
		navigationOptions: ({ navigation }) => {
			return {
				gesturesEnabled: false,
				headerStyle: {
					backgroundColor: theme.COLOR_GREEN
				},
				headerTintColor: "#fff",
				headerRight: (
					<Button transparent onPress={() => navigation.navigate("User")}>
						<Icon style={{ color: "#fff" }} type="Entypo" name="user" />
					</Button>
				)
			}
		}
	}
)

export default class NavWrapper extends Component {
	render() {
		return (
			<Query query={USER_SIGNED_IN_QUERY}>
				{({ loading, error, data }) => {
					if (loading) return <ActivityIndicator size="large" style={styles.spinner} />
					if (error) return `Error! ${error}`

					if (data.user === null) {
						return <Login />
					} else {
						return <Navigator screenProps={data} />
					}
				}}
			</Query>
		)
	}
}

const styles = StyleSheet.create({
	spinner: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0
	}
})
