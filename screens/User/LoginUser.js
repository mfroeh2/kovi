import React, { Component } from "react"
import { Text, View, StyleSheet } from "react-native"
import { H2 } from "native-base"
import { graphql } from "react-apollo"
import gql from "graphql-tag"

import UserForm from "./UserForm"
import { signIn } from "./loginUtils"

class LoginUser extends Component {
	loginUser = async ({ email, password }) => {
		this.props.onSubmit()
		try {
			const signin = await this.props.signinUser({
				variables: { email, password }
			})
			signIn(signin.data.signinUser.token)
			this.props.client.resetStore()
		} catch (e) {
			console.log(e)
		}
	}

	render() {
		return (
			<View>
				<H2 style={styles.text}>Login</H2>
				<UserForm onSubmit={this.loginUser} type="Login" />
			</View>
		)
	}
}

const signinUser = gql`
	mutation signinUser($email: String!, $password: String!) {
		signinUser(email: { email: $email, password: $password }) {
			token
		}
	}
`

const styles = StyleSheet.create({
	text: {
		marginLeft: 20,
		textAlign: "center",
		color: "#fff",
		fontWeight: "100",
		fontSize: 45,
		lineHeight: 45
	}
})

export default graphql(signinUser, { name: "signinUser" })(LoginUser)
