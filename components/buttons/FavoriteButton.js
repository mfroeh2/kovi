import React, { Component } from "react"
import { Query, Mutation } from "react-apollo"

import ActionButton from "./ActionButton"
import IS_FAVORITE from "../../graphql/queries/IsFavorite"
import ADD_FAVORITE from "../../graphql/mutations/AddFavorite"
import REMOVE_FAVORITE from "../../graphql/mutations/RemoveFavorite"

export default class FavoriteButton extends Component {
	state = {
		isFavorite: false
	}

	render() {
		const { propertyID, userID } = this.props

		return (
			<Query query={IS_FAVORITE} variables={{ id: userID, propertyId: propertyID }}>
				{({ loading, error, data }) => {
					if (loading) return <ActionButton icon="star-outlined">Favorite</ActionButton>

					if (error) return console.log(error)

					if (data.User.favoriteProperties[0]) {
						return (
							<Mutation mutation={REMOVE_FAVORITE} variables={{ id: data.User.favoriteProperties[0].id }} refetchQueries={["User", "UserDetails"]}>
								{(deleteFavoriteProperty, { data }) => {
									return (
										<ActionButton
											icon="star"
											onPress={() => {
												this.setState({ isFavorite: false })
												deleteFavoriteProperty()
											}}
										>
											Favorite
										</ActionButton>
									)
								}}
							</Mutation>
						)
					} else if (!this.state.isFavorite) {
						return (
							<Mutation mutation={ADD_FAVORITE} variables={{ userId: userID, propertyId: propertyID }} refetchQueries={["User", "UserDetails"]}>
								{(createFavoriteProperty, { data }) => {
									return (
										<ActionButton
											icon="star-outlined"
											onPress={() => {
												this.setState({ isFavorite: true })
												createFavoriteProperty()
											}}
										>
											Favorite
										</ActionButton>
									)
								}}
							</Mutation>
						)
					} else {
						return <ActionButton icon="star">Favorite</ActionButton>
					}
				}}
			</Query>
		)
	}
}
