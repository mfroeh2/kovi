import React, { Component } from "react"
import { Text, View, StyleSheet } from "react-native"

import FavoriteButton from "../../components/buttons/FavoriteButton"
import Nearby from "../../components/buttons/Nearby"
import CheckIn from "../../components/buttons/CheckIn"
import PropertyNotesButton from "../PropertyNotes/PropertyNotesButton"

export default class ActionButtons extends Component {
	render() {
		const { property, user, navigation } = this.props

		return (
			<View style={styles.buttonRow}>
				<FavoriteButton propertyID={property.id} userID={user.id} />
				<Nearby navigation={navigation} coords={property.coordinates} />
				<CheckIn property={property} user={user} />
				<PropertyNotesButton navigation={navigation} userID={user.id} property={property} />
			</View>
		)
	}
}

const styles = StyleSheet.create({
	buttonRow: {
		flexDirection: "row",
		justifyContent: "space-between"
	}
})
