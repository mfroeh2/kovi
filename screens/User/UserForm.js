import React, { Component } from "react"
import { Text, View, StyleSheet } from "react-native"
import { Button } from "native-base"
import { Form, Item, Input, Label } from "native-base"
import theme from "../../styles/theme"

export default class UserForm extends Component {
	state = {
		email: "",
		password: ""
	}

	submitForm = () => {
		const { email, password } = this.state
		this.props.onSubmit({
			email,
			password
		})
	}

	render() {
		return (
			<Form>
				<Item floatingLabel style={styles.formField}>
					<Label style={styles.label}>Email</Label>
					<Input keyboardType="email-address" value={this.state.email} onChangeText={email => this.setState({ email })} style={styles.input} />
				</Item>
				<Item floatingLabel style={styles.formField}>
					<Label style={styles.label}>Password</Label>
					<Input secureTextEntry value={this.state.password} onChangeText={password => this.setState({ password })} style={styles.input} />
				</Item>
				<Button block style={styles.button} onPress={this.submitForm}>
					<Text style={styles.buttonText}>{this.props.type}</Text>
				</Button>
			</Form>
		)
	}
}

const styles = StyleSheet.create({
	button: {
		backgroundColor: theme.COLOR_GREEN,
		margin: 20
	},
	buttonText: {
		color: "#fff",
		fontWeight: "bold"
	},
	formField: {
		marginRight: 15
	},
	label: {
		color: "#fff"
	},
	input: {
		color: "#fff"
	}
})
