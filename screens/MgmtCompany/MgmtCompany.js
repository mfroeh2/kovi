import React, { Component } from "react"
import { View, Text, ActivityIndicator, FlatList, StyleSheet } from "react-native"
import { Query } from "react-apollo"
import { Container, Content, List, ListItem, Body, Right, Icon, H3, Card, CardItem } from "native-base"

import MGMTCO_DETAILS from "../../graphql/queries/MgmtCoDetails"
import FooterTabs from "../../layout/FooterTabs"
import PropertiesList from "../../components/PropertiesList"

export default class MgmtCompany extends Component {
	render() {
		const { navigation } = this.props

		return (
			<Container>
				<Content style={styles.content}>
					<Query query={MGMTCO_DETAILS} variables={{ id: navigation.state.params.id }}>
						{({ loading, error, data }) => {
							if (loading) return <ActivityIndicator size="large" style={styles.spinner} />
							if (error) return console.log(error)

							return (
								<View>
									<Card style={styles.card}>
										<CardItem>
											<Body style={styles.companyName}>
												<H3>{navigation.state.params.name}</H3>
												<Text style={styles.small}>Total Properties: {data.MgmtCo.properties.length}</Text>
											</Body>
										</CardItem>
									</Card>
									<PropertiesList data={data.MgmtCo.properties} navigation={navigation} />
								</View>
							)
						}}
					</Query>
				</Content>
				<FooterTabs navigation={this.props.navigation} />
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	content: {
		backgroundColor: "#ededed"
	},
	card: {
		width: "90%",
		marginLeft: "5%",
		marginTop: 10,
		marginBottom: 10,
		padding: 15
	},
	companyName: {
		alignItems: "center"
	},
	small: {
		fontSize: 12,
		color: "rgba(51,55,69,.5)",
		marginTop: 5
	},
	spinner: {
		marginTop: "70%"
	}
})
