import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { Button, Icon, Badge } from 'native-base';
import { Query } from 'react-apollo';

import GET_NOTES from '../../graphql/queries/GetPropertyNotes';
import theme from '../../styles/theme';

PropertyNotesButton = (props) => {

    const { navigation, userID, property } = props;

    goToPropertyNotes = ({allPropertyNotes}) => {
        navigation.navigate('PropertyNotes',
        {
            propertyName: property.propertyName,
            id: property.id,
            allPropertyNotes
        });
    }

    return (
            <Query
                query={GET_NOTES}
                variables={{
                    propertyId: property.id,
                    userId: userID
            }}>
                {({data, loading, error}) => {
                    if (loading) return (
                        <Button onPress={() => this.goToPropertyNotes(data)} style={styles.actionBtn}>
                            <Icon name="new-message" type="Entypo" style={styles.actionBtnIcon} />
                            <Text style={styles.actionBtnText}>Notes</Text>
                        </Button>
                    )

                    if (error) return console.log(error);

                    return (
                        <Button badge onPress={() => this.goToPropertyNotes(data)} style={styles.actionBtn}>
                            {data.allPropertyNotes.length &&
                                <Badge style={styles.badge}>
                                    <Text style={styles.badgeText}>{data.allPropertyNotes.length}</Text>
                                </Badge>
                            }
                            <Icon name="new-message" type="Entypo" style={styles.actionBtnIcon} />
                            <Text style={styles.actionBtnText}>Notes</Text>
                        </Button>
                    )
                }}
            </Query>

    );
}

const styles = StyleSheet.create({
    actionBtn: {
        backgroundColor: theme.COLOR_ORANGE,
        width: '24%',
        flexDirection: 'column',
        height: 55
    },
    actionBtnText: {
        color: '#fff',
        fontWeight: '500',
        marginTop: 0
    },
    actionBtnIcon: {
        fontSize: 20
    },
    badge: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        top: -12,
        right: 5,
        backgroundColor: theme.COLOR_GREEN,
        width: 25,
        height: 25
    },
    badgeText: {
        color: '#fff',
        fontWeight: 'bold'
    }
})

export default PropertyNotesButton;
