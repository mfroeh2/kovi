import React, { Component } from "react"
import { StyleSheet, Text } from "react-native"
import { withApollo } from "react-apollo"
import { Container, Content, Button } from "native-base"

import theme from "../../styles/theme"
import FooterTabs from "../../layout/FooterTabs"
import { signOut } from "./loginUtils"

class User extends Component {
	static navigationOptions = {
		title: "User"
	}

	render() {
		return (
			<Container style={styles.container}>
				<Content>
					<Button
						full
						style={styles.button}
						onPress={() => {
							signOut()
							this.props.client.resetStore()
						}}
					>
						<Text style={styles.buttonText}>Logout</Text>
					</Button>
				</Content>
				<FooterTabs navigation={this.props.navigation} />
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "flex-end"
	},
	button: {
		backgroundColor: theme.COLOR_ORANGE
	},
	buttonText: {
		color: "#fff",
		fontWeight: "bold"
	}
})

export default withApollo(User)
