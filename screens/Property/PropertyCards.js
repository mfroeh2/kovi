import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableHighlight, Alert } from "react-native"
import { Card, CardItem, Icon, Body, H3 } from "native-base"
import { MailComposer } from "expo"
import { Mutation } from "react-apollo"
import * as Animatable from "react-native-animatable"

import call from "react-native-phone-call"
import { showLocation } from "react-native-map-link"

import theme from "../../styles/theme"
import CREATE_CONTACT_EVENT from "../../graphql/mutations/CreateContactEvent"
import GET_USER_DETAILS from "../../graphql/queries/GetUserDetails"

Animatable.Card = Animatable.createAnimatableComponent(Card)

export default class PropertyCards extends Component {
	delay = 0

	delayPlus = () => (this.delay += 200)

	openMap = () => {
		const { Property } = this.props
		showLocation({
			latitude: Property.coordinates.latitude,
			longitude: Property.coordinates.longitude,
			title: Property.propertyName,
			googleForceLatLon: false, // optionally force GoogleMaps to use the latlon for the query instead of the title
			dialogTitle: "Open in Maps", // optional (default: 'Open in Maps')
			dialogMessage: "Choose your preferred maps app", // optional (default: 'What app would you like to use?')
			cancelText: "Cancel" // optional (default: 'Cancel')
		})
	}

	call = () => {
		const args = {
			number: `${this.props.Property.phone}`,
			prompt: true
		}

		call(args).catch(console.error)
	}

	composeEmail = async (recipient, createContactEvent) => {
		const emailStatus = await MailComposer.composeAsync({
			recipients: [recipient],
			subject: "",
			body: ""
		})

		if (emailStatus.status === "sent") {
			createContactEvent()
		}
	}

	goToMgmtCo = ({ id, name }) => {
		this.props.navigation.navigate("MgmtCompany", { id, name })
	}

	render() {
		const { Property, userId } = this.props

		return (
			<View>
				{Property.address && (
					<Animatable.Card useNativeDriver={true} delay={this.delayPlus()} animation="fadeInLeft" style={styles.infoCard}>
						<CardItem header style={styles.cardHeader}>
							<H3>Address</H3>
						</CardItem>
						<CardItem>
							<Body style={styles.infoBody}>
								<TouchableHighlight onPress={this.openMap} activeOpacity={0.85} underlayColor={theme.COLOR_GREEN_OPACITY}>
									<View>
										<Text style={styles.link}>{Property.address.street}</Text>
										<Text style={styles.link}>{`${Property.address.city}, ${Property.address.state} ${Property.address.zip}`}</Text>
									</View>
								</TouchableHighlight>
							</Body>
						</CardItem>
					</Animatable.Card>
				)}

				{Property.mgmtCo && (
					<Animatable.Card useNativeDriver={true} delay={this.delayPlus()} animation="fadeInLeft" style={styles.infoCard}>
						<CardItem header style={styles.cardHeader}>
							<H3>Management Co.</H3>
						</CardItem>
						<CardItem>
							<Body style={styles.infoBody}>
								<TouchableHighlight onPress={() => this.goToMgmtCo(Property.mgmtCo)} activeOpacity={0.85} underlayColor={theme.COLOR_GREEN_OPACITY}>
									<Text style={styles.link}>{Property.mgmtCo.name}</Text>
								</TouchableHighlight>
							</Body>
						</CardItem>
					</Animatable.Card>
				)}

				{(Property.phone || Property.email) && (
					<View style={styles.cardRow}>
						{Property.phone && (
							<Animatable.Card useNativeDriver={true} delay={this.delayPlus()} animation="fadeInLeft" style={styles.infoCardHalf}>
								<CardItem>
									<Body style={styles.contactLinksBody}>
										<Mutation
											mutation={CREATE_CONTACT_EVENT}
											variables={{
												type: "CALL",
												userId,
												propertyId: Property.id
											}}
											refetchQueries={() => [
												{
													query: GET_USER_DETAILS,
													variables: {
														id: user.id
													}
												}
											]}
										>
											{(createContactEvent, { data }) => (
												<TouchableHighlight
													onPress={() => {
														this.call()
														createContactEvent()
													}}
													activeOpacity={0.85}
													underlayColor={theme.COLOR_GREEN_OPACITY}
												>
													<View>
														<CardItem header style={styles.cardHeader}>
															<Icon name="phone" type="Entypo" style={styles.icon} />
														</CardItem>

														<Text style={[styles.link, styles.contact]}>{Property.phone}</Text>
													</View>
												</TouchableHighlight>
											)}
										</Mutation>
									</Body>
								</CardItem>
							</Animatable.Card>
						)}
						{Property.email && (
							<Animatable.Card useNativeDriver={true} delay={this.delayPlus()} animation="fadeInLeft" style={styles.infoCardHalf}>
								<CardItem>
									<Body style={styles.contactLinksBody}>
										<Mutation
											mutation={CREATE_CONTACT_EVENT}
											variables={{
												type: "EMAIL",
												userId,
												propertyId: Property.id
											}}
											refetchQueries={() => [
												{
													query: GET_USER_DETAILS,
													variables: {
														id: userId
													}
												}
											]}
										>
											{(createContactEvent, { data }) => (
												<TouchableHighlight
													onPress={() => {
														this.composeEmail(Property.email, createContactEvent)
													}}
													activeOpacity={0.85}
													underlayColor={theme.COLOR_GREEN_OPACITY}
												>
													<View>
														<CardItem header style={styles.cardHeader}>
															<Icon name="mail" type="Entypo" style={styles.icon} />
														</CardItem>

														<Text style={[styles.link, styles.contact]}>{Property.email}</Text>
													</View>
												</TouchableHighlight>
											)}
										</Mutation>
									</Body>
								</CardItem>
							</Animatable.Card>
						)}
					</View>
				)}

				{(Property.onSiteMgr || Property.regionalMgr) && (
					<Animatable.Card useNativeDriver={true} delay={this.delayPlus()} animation="fadeInLeft" style={styles.infoCard}>
						<CardItem header style={styles.cardHeader}>
							<H3>Staff</H3>
						</CardItem>
						<CardItem>
							<Body style={styles.infoBody}>
								{Property.onSiteMgr && (
									<View style={styles.infoRow}>
										<Text style={styles.infoKey}>Onsite Manager:</Text>
										<Text>{Property.onSiteMgr}</Text>
									</View>
								)}
								{Property.regionalMgr && (
									<View style={styles.infoRow}>
										<Text style={styles.infoKey}>Regional Manager:</Text>
										<Text>{Property["regionalMgr"]}</Text>
									</View>
								)}
								{Property.maintenanceSupervisor && (
									<View style={styles.infoRow}>
										<Text style={styles.infoKey}>Maintenance Supervisor:</Text>
										<Text>{Property.maintenanceSupervisor}</Text>
									</View>
								)}
							</Body>
						</CardItem>
					</Animatable.Card>
				)}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	infoCard: {
		marginTop: 10
	},
	infoCardHalf: {
		flex: 1,
		marginTop: 10
	},
	cardHeader: {
		justifyContent: "center",
		alignSelf: "center",
		marginBottom: 3,
		paddingBottom: 7,
		borderBottomColor: "rgba(51,59,59,0.3)",
		borderBottomWidth: StyleSheet.hairlineWidth,
		width: "80%",
		backgroundColor: "transparent"
	},
	infoBody: {
		alignItems: "center",
		paddingTop: 0,
		paddingBottom: 20
	},
	infoKey: {
		fontWeight: "700",
		marginRight: 10,
		marginTop: 2,
		marginBottom: 2,
		textAlign: "right"
	},
	infoRow: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	textRow: {
		paddingTop: 5,
		paddingBottom: 5
	},
	icon: {
		fontSize: 30,
		textDecorationLine: "none",
		textAlign: "center",
		color: "#333745"
	},
	link: {
		color: "#1b998b",
		textDecorationLine: "underline"
	},
	cardRow: {
		flexDirection: "row"
	},
	contactLinksBody: {
		paddingTop: 10,
		paddingBottom: 10,
		alignItems: "center"
	},
	contact: {
		marginTop: 15
	}
})
