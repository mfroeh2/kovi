import gql from 'graphql-tag';

const CREATE_CONTACT_EVENT = gql`
    mutation AddContactEvent($type: EVENTTYPE!, $propertyId: ID!, $userId: ID!) {
        createContactEvent(type: $type, propertyId: $propertyId, userId: $userId) {
            id
        }
    }
`;

export default CREATE_CONTACT_EVENT;