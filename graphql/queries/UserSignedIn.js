import gql from 'graphql-tag';

const USER_SIGNED_IN_QUERY = gql`
  query userQuery {
    user {
        id
        email
        role
    }
  }
`;

export default USER_SIGNED_IN_QUERY;