import React from "react"
import { View, StyleSheet } from "react-native"
import { Header } from "react-navigation"

const TransparentHeader = props => {
	return (
		<View style={styles.header}>
			<Header {...props} />
		</View>
	)
}

const styles = StyleSheet.create({
	header: {
		backgroundColor: "transparent",
		position: "absolute",
		top: 0,
		left: 0,
		right: 0,
		borderBottomWidth: 0
	}
})

export default TransparentHeader
