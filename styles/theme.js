export default {
    COLOR_GREEN: "#44e5b2",
    COLOR_GREY: '#333745',
    COLOR_ORANGE: '#fe4a49',
    COLOR_GREEN_OPACITY: 'rgba(68,229,178,0.3)'
}