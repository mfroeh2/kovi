import React from 'react'
import { Text, StyleSheet } from 'react-native'
import { ListItem, Body, Right, Icon } from 'native-base';

const PropertiesListItem = ({navigation, item}) => {

    return (
        <ListItem
            onPress={() => navigation.navigate('Property', {
                id: item.id,
                propertyName: item.propertyName
            })}
            style={styles.listItem}
        >
            <Body>
                <Text>{item.propertyName}</Text>
            </Body>
            <Right>
                <Icon name="arrow-forward"v/>
            </Right>
        </ListItem>
    )
}

const styles = StyleSheet.create({
    listItem: {
        marginLeft: 0,
        paddingLeft: 15,
        backgroundColor: '#fff',
        width: '100%',
    }
})

export default PropertiesListItem;