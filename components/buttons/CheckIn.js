import React, { Component } from "react"
import { Alert } from "react-native"
import { Mutation } from "react-apollo"

import ActionButton from "./ActionButton"
import CREATE_CONTACT_EVENT from "../../graphql/mutations/CreateContactEvent"
import GET_USER_DETAILS from "../../graphql/queries/GetUserDetails"

export default class CheckIn extends Component {
	state = {
		currentLocation: {
			latitude: 0,
			longitude: 0
		}
	}

	getCurrentLocation = () => {
		const options = {
			enableHighAccuracy: true,
			timeout: 5000,
			maximumAge: 0
		}

		success = position => {
			this.setState({
				currentLocation: {
					latitude: position.coords.latitude,
					longitude: position.coords.longitude
				}
			})

			this.isUserClose()
		}

		error = err => {
			console.warn(`ERROR(${err.code}): ${err.message}`)
		}

		navigator.geolocation.getCurrentPosition(success, error, options)
	}

	isUserClose = () => {
		return (
			this.state.currentLocation.latitude === this.props.property.coordinates.latitude &&
			this.state.currentLocation.longitude === this.props.property.coordinates.longitude
		)
	}

	render() {
		const { property, user } = this.props

		return (
			<Mutation
				mutation={CREATE_CONTACT_EVENT}
				variables={{
					type: "VISIT",
					propertyId: property.id,
					userId: user.id
				}}
				refetchQueries={() => [
					{
						query: GET_USER_DETAILS,
						variables: {
							id: user.id
						}
					}
				]}
			>
				{(createContactEvent, { data }) => (
					<ActionButton
						icon="key"
						onPress={() => {
							if (this.getCurrentLocation()) {
								createContactEvent()
								Alert.alert("Check-In Successful!")
							} else {
								Alert.alert("Sorry, Check-In was unsuccessful", "You must be at the property to check-in...")
							}
						}}
					>
						Check-In
					</ActionButton>
				)}
			</Mutation>
		)
	}
}
