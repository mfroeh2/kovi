import React, { Component } from "react"
import { View, Text, ActivityIndicator, StyleSheet, Dimensions, ImageBackground } from "react-native"
import { Container, Content, Card, CardItem, Body, H3 } from "native-base"
import { Query } from "react-apollo"
import * as Animatable from "react-native-animatable"

import theme from "../../styles/theme"
import FooterTabs from "../../layout/FooterTabs"
import PropertyCards from "./PropertyCards"
import ActionButtons from "./ActionButtons"

import PROPERTY_DETAILS from "../../graphql/queries/PropertyDetails"

export default class Property extends Component {
	state = {
		nameCard: {
			top: 0
		},
		infoContainer: {
			paddingTop: 0
		}
	}

	nameHeight = event => {
		const { height } = event.nativeEvent.layout
		this.setState({
			nameCard: {
				top: vh(30) - (height / 2 + 5)
			},
			infoContainer: {
				paddingTop: height / 2 + 10
			}
		})
	}

	render() {
		const { navigation } = this.props

		const { user } = this.props.screenProps

		return (
			<Container>
				<Content style={styles.container}>
					<Query query={PROPERTY_DETAILS} variables={{ id: navigation.state.params.id }}>
						{({ loading, error, data }) => {
							const { Property } = data

							if (loading) return <ActivityIndicator size="large" style={styles.spinner} />
							if (error) return console.log(error)

							return (
								<View>
									<View style={styles.imageContainer}>
										<ImageBackground resizeMode="cover" style={styles.image} source={require("../../assets/images/apartment-placeholder-min.jpg")} />

										<Animatable.View useNativeDriver={true} animation="fadeInLeft" style={[this.state.nameCard, styles.nameCard]}>
											<Card onLayout={event => this.nameHeight(event)}>
												<CardItem>
													<Body style={styles.propertyName}>
														<H3 style={styles.propertyTitle}>{Property.propertyName}</H3>
														<Text style={styles.small}>{Property.units} units</Text>
													</Body>
												</CardItem>
											</Card>
										</Animatable.View>
									</View>
									<View style={[this.state.infoContainer, styles.infoContainer]}>
										<ActionButtons property={Property} user={user} navigation={navigation} />

										<PropertyCards Property={data.Property} userId={user.id} navigation={navigation} />
									</View>
								</View>
							)
						}}
					</Query>
				</Content>

				<FooterTabs activeBtn="properties" navigation={navigation} />
			</Container>
		)
	}
}

const vh = percentage => {
	return Dimensions.get("window").height * (percentage / 100)
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	image: {
		backgroundColor: theme.COLOR_GREEN,
		flex: 1,
		position: "absolute",
		width: "100%",
		height: "100%",
		alignItems: "center"
	},
	imageContainer: {
		backgroundColor: theme.COLOR_GREEN,
		height: vh(30),
		width: "100%",
		alignItems: "center",
		zIndex: 100
	},
	nameCard: {
		width: "80%",
		position: "absolute"
	},
	propertyName: {
		alignItems: "center",
		padding: 10
	},
	propertyTitle: {
		color: theme.COLOR_GREY,
		fontWeight: "200",
		fontSize: 26,
		lineHeight: 26,
		textAlign: "center"
	},
	infoContainer: {
		backgroundColor: "#ededed",
		paddingBottom: 50,
		paddingLeft: "1%",
		paddingRight: "1%"
	},
	small: {
		fontSize: 12,
		color: "rgba(51,55,69,.5)",
		marginTop: 5
	},
	spinner: {
		marginTop: "70%"
	}
})
