import gql from 'graphql-tag';

const DELETE_PROPERTY_NOTE = gql`
mutation deletePropertyNote($id: ID!) {
    deletePropertyNote(id: $id) {
        id
    }
}`;

export default DELETE_PROPERTY_NOTE;