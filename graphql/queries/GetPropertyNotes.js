import gql from 'graphql-tag';

const GET_NOTES = gql`
query getNotes($propertyId: ID!, $userId: ID!) {
    allPropertyNotes(filter: {
      AND: [
        {property: {
        id: $propertyId
      }}, {user: {
        id: $userId
      }}
      ]}, orderBy: createdAt_DESC) {
        id
        title
        body
        createdAt
    }
}`;

export default GET_NOTES;