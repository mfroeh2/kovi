import React, { Component } from "react"
import { View } from "react-native"
import Modal from "react-native-modal"
import { graphql } from "react-apollo"
import gql from "graphql-tag"

import NoteForm from "../../components/NoteForm"

class NewPropertyNote extends Component {
	state = {
		loading: false
	}

	newNote = ({ title, body }) => {
		const { user, toggleModal, propertyId } = this.props
		this.props
			.newPropertyNote({
				variables: {
					title,
					body,
					userId: user.id,
					propertyId
				}
			})
			.then(() => {
				toggleModal()
			})
			.catch(error => {
				console.log(error)
			})
	}

	render() {
		const { toggleModal, visible } = this.props
		return (
			<View>
				<Modal animationType="slide" transparent={true} isVisible={visible} avoidKeyboard={true}>
					<NoteForm onSubmit={this.newNote} toggleModal={toggleModal} />
				</Modal>
			</View>
		)
	}
}

const newPropertyNote = gql`
	mutation newPropertyNote($title: String, $body: String!, $userId: ID!, $propertyId: ID!) {
		createPropertyNote(title: $title, body: $body, userId: $userId, propertyId: $propertyId) {
			id
		}
	}
`

export default graphql(newPropertyNote, {
	name: "newPropertyNote",
	options: {
		refetchQueries: ["getNotes"]
	}
})(NewPropertyNote)
