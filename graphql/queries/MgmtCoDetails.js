import gql from 'graphql-tag';

const MGMTCO_DETAILS = gql`
    query MgmtCo($id: ID!) {
        MgmtCo(id: $id) {
            id
            name
            properties {
                id
                propertyName
            }
        }
    }
`;

export default MGMTCO_DETAILS