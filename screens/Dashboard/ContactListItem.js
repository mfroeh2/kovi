import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { ListItem, Left, Body, Right, Icon } from 'native-base'

import { FormattedDate } from 'react-native-globalize';
import theme from '../../styles/theme';


const ContactListItem = (props) => {

    const { item } = props;

    return (
        <ListItem
            style={styles.listItem}
            onPress={() => props.navigation.navigate('Property', {
                id: item.property.id,
                propertyName: item.property.propertyName
            })}
        >
            <Left>
                <Text style={styles.text}>{item.property.propertyName}</Text>
            </Left>
            <Body>
                <FormattedDate
                    value={new Date(item.createdAt)}
                    style={[styles.small, styles.date]}
                    datetime='short'
                />
            </Body>
            <Right>
                <Icon name="arrow-forward" />
            </Right>
        </ListItem>
    )
}


const styles = StyleSheet.create({
    text: {
        color: theme.COLOR_GREY
    },
    list: {
        width: '100%'
    },
    listItem: {
        marginLeft: 0,
        paddingLeft: 15,
        backgroundColor: '#fff',
        width: '100%',
    },
    small: {
        fontSize: 10,
        color: 'rgba(0,0,0,0.3)',
    },
    date: {
        textAlign: 'right'
    }
})

export default ContactListItem;